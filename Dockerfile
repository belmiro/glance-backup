FROM centos:centos7
MAINTAINER moreira.belmiro@gmail.com

RUN printf '[ceph-nautilus] \nname=Ceph Nautilus\nbaseurl=http://linuxsoft.cern.ch/mirror/download.ceph.com/rpm-nautilus/el7/x86_64 \nenabled=1 \ngpgcheck=0 \npriority=1 \n' \
    > /etc/yum.repos.d/ceph-nautilus.repo
RUN printf '[epel] \nname=epel\nbaseurl=http://linuxsoft.cern.ch/epel/7/x86_64 \nenabled=1 \ngpgcheck=0 \npriority=1 \n' \
    > /etc/yum.repos.d/epel.repo

RUN yum clean all; yum update -y
RUN yum install -y ceph-common python-rbd vim-minimal MySQL-python python2-PyMySQL python-boto3 python-prettytable python-sqlalchemy

COPY glance-backup.py /usr/bin/glance-backup
RUN chmod +x /usr/bin/glance-backup
RUN rm -rf /etc/ceph